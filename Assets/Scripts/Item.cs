﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public enum EItemType { eJewel, eChest, eHeart, eKey};
    public EItemType ItemType { get; set; }
    public EnvironmentTile CurrentPosition { get; set; }


}
