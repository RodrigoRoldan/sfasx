﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Game : MonoBehaviour
{
    [SerializeField] private Camera MainCamera;
    [SerializeField] private Character Character1;
    [SerializeField] private Character Character2;
    [SerializeField] private Character Character3;
    [SerializeField] private Character Character4;
    [SerializeField] private Character Enemy;
    [SerializeField] private Item Jewel;
    [SerializeField] private Item Key;
    [SerializeField] private Item Heart;
    [SerializeField] private Item Chest;
    [SerializeField] private Canvas Menu;
    [SerializeField] private Canvas Hud;
    [SerializeField] private TMPro.TextMeshPro HelpTxt;
    [SerializeField] private Transform CharacterStart;
    [SerializeField] private Transform EnemyStart;

    private RaycastHit[] mRaycastHits;
    private List<Character> mCharacters;
    private List<Item> mItems;
    private Environment mMap;

    private static int mCurrentCharacter = 0;
    private static float mTime = 0.0f;
    private static int mScore = 0;

    private readonly int NumberOfRaycastHits = 1;

    private void Awake()
    {
        mCharacters = new List<Character>();
        mItems = new List<Item>();
    }
    void Start()
    {
        mRaycastHits = new RaycastHit[NumberOfRaycastHits];
        mMap = GetComponentInChildren<Environment>();
        LoadCharacters();
        LoadItems();
        ShowMenu(true);
        mCharacters[mCurrentCharacter].UI.GetComponentInChildren<TMPro.TextMeshProUGUI>().fontSize = 24;
    }

    private void Update()
    {
        // Check to see if the player has clicked a tile and if they have, try to find a path to that 
        // tile. If we find a path then the character will move along it to the clicked tile. 
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            mCharacters[mCurrentCharacter].UI.GetComponentInChildren<TMPro.TextMeshProUGUI>().fontSize = 14;
            mCurrentCharacter = 0;
            mCharacters[mCurrentCharacter].UI.GetComponentInChildren<TMPro.TextMeshProUGUI>().fontSize = 24;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            mCharacters[mCurrentCharacter].UI.GetComponentInChildren<TMPro.TextMeshProUGUI>().fontSize = 14;
            mCurrentCharacter = 1;
            mCharacters[mCurrentCharacter].UI.GetComponentInChildren<TMPro.TextMeshProUGUI>().fontSize = 24;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            mCharacters[mCurrentCharacter].UI.GetComponentInChildren<TMPro.TextMeshProUGUI>().fontSize = 14;
            mCurrentCharacter = 2;
            mCharacters[mCurrentCharacter].UI.GetComponentInChildren<TMPro.TextMeshProUGUI>().fontSize = 24;
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            mCharacters[mCurrentCharacter].UI.GetComponentInChildren<TMPro.TextMeshProUGUI>().fontSize = 14;
            mCurrentCharacter = 3;
            mCharacters[mCurrentCharacter].UI.GetComponentInChildren<TMPro.TextMeshProUGUI>().fontSize = 24;
        }
        if(Input.GetMouseButtonDown(0) && mCharacters[mCurrentCharacter].CharacterType == Character.EType.ePlayer && mCharacters[mCurrentCharacter].isAlive)
        {
            Ray screenClick = MainCamera.ScreenPointToRay(Input.mousePosition);
            int hits = Physics.RaycastNonAlloc(screenClick, mRaycastHits);
            if( hits > 0)
            {
                EnvironmentTile tile = mRaycastHits[0].transform.GetComponent<EnvironmentTile>();
                if (tile != null)
                {
                    mCharacters[mCurrentCharacter].mRoute = mMap.Solve(mCharacters[mCurrentCharacter].CurrentPosition, tile);
                    mCharacters[mCurrentCharacter].GoTo(mCharacters[mCurrentCharacter].mRoute);
                }
            }
        }
        //for the enemy:  
        mTime += Time.deltaTime;
        if(mTime > 5.0f)
        {
            HelpTxt.text = "";
        }
        for (int i = mCharacters.Count - 1; i < mCharacters.Count; i++) //enemies
        {
            bool changePath = false;
            if (mCharacters[i].mRoute == null || mTime > 5.0f)
            {
                mCharacters[i].mRoute = mMap.Solve(mCharacters[i].CurrentPosition, mCharacters[0].CurrentPosition);
                mTime = 0.0f;
            }                
            for (int j = 0; j < mCharacters.Count - 1; j++) //players
            {
                if (mCharacters[j].isAlive)
                {
                    List<EnvironmentTile> tempRoute = new List<EnvironmentTile>();
                    tempRoute = mMap.Solve(mCharacters[i].CurrentPosition, mCharacters[j].CurrentPosition);
                    if (tempRoute == null)
                    {
                        mCharacters[j].CheckCollisions(mCharacters[i]);
                    }
                    if (tempRoute != null)
                    {
                        if (tempRoute.Count < mCharacters[i].mRoute.Count)
                        {
                            mCharacters[i].mRoute = tempRoute;
                            changePath = true;
                        }
                    }                      
                }                                 
            }            
            if(changePath )
            {
                mCharacters[i].GoTo(mCharacters[i].mRoute);
            }
        }            
        //Collisions
        for (int i = 0; i < mCharacters.Count; i++)
        {
            for (int j = i + 1; j < mCharacters.Count; j++)
            {
                if(mCharacters[i].CheckCollisions(mCharacters[j]))
                {
                    if (mCharacters[i].isAlive)
                    {
                        HelpTxt.text = "Player Revived";
                    }
                    else
                    {
                        HelpTxt.text = "Give a heart to P" + (i + 1) + " to revive";
                    }
                }
            }
            foreach (Item item in mItems)
            {
                if(mCharacters[i].CheckCollisions(item))
                {
                    if(item.ItemType != Item.EItemType.eChest)
                    {
                        mScore += 100;
                    }                    
                    if (item.ItemType == Item.EItemType.eChest && !mCharacters[i].hasTheKey)
                    {
                        HelpTxt.text = "You need the key to open the chest";
                    }
                    if (item.ItemType == Item.EItemType.eChest && mCharacters[i].hasTheKey)
                    {
                        mScore += 400;
                        ShowMenu(true);
                    }
                    
                    Hud.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Score: " + mScore;                    
                }
                if(item.name.Contains("Key"))
                {
                    //move the key to the last enemy position
                    item.CurrentPosition = mCharacters[mCharacters.Count - 1].PreviousPosition;
                    item.transform.position = Vector3.Lerp(item.CurrentPosition.Position, mCharacters[mCharacters.Count - 1].CurrentPosition.Position, mTime/1.0f);                    
                }
            }
        }
    }

    public void ShowMenu(bool show)
    {
        if (Menu != null && Hud != null)
        {
            Menu.enabled = show;
            Hud.enabled = !show;

            if( show )
            {
                //FOR THE MAIN MENU
                HelpTxt.text = "";
                Vector3 tempPos = CharacterStart.position;
                foreach (Character character in mCharacters)
                {
                    character.StopAllCoroutines();
                    if (character.CharacterType == Character.EType.ePlayer)
                    {                        
                        character.transform.position = tempPos;
                        character.transform.rotation = CharacterStart.rotation;
                        tempPos.x -= 5;
                        character.ResetValues();
                    }
                    else
                    {
                        character.transform.position = EnemyStart.position;
                        character.transform.rotation = EnemyStart.rotation;
                    }
                }
                foreach (Item item in mItems)
                {
                    Renderer[] tempRender = item.GetComponentsInChildren<Renderer>();
                    foreach (Renderer rn in tempRender)
                    {
                        rn.enabled = false;
                    }
                }
                mMap.CleanUpWorld();
            }
            else
            {
                //WHEN THE GAME STARTS
                HelpTxt.text = "Open the chest to go to the next level";
                for (int i = 0; i < mCharacters.Count; i++)
                {                    
                    mCharacters[i].transform.rotation = Quaternion.identity;
                    switch (i)
                    {
                        case 0:
                            mCharacters[i].transform.position = mMap.BottomLeft.Position;
                            mCharacters[i].CurrentPosition = mMap.BottomLeft;
                            break;
                        case 1:
                            mCharacters[i].transform.position = mMap.TopLeft.Position;
                            mCharacters[i].CurrentPosition = mMap.TopLeft;
                            break;
                        case 2:
                            mCharacters[i].transform.position = mMap.TopRight.Position;
                            mCharacters[i].CurrentPosition = mMap.TopRight;
                            break;
                        case 3:
                            mCharacters[i].transform.position = mMap.BottomRight.Position;
                            mCharacters[i].CurrentPosition = mMap.BottomRight;
                            break;
                        default:
                            mCharacters[i].transform.position = mMap.Middle.Position;
                            mCharacters[i].CurrentPosition = mMap.Middle;
                            break;
                    }                    
                }
                //HelpTxt.enabled = true;
            }
        }
    }

    public void LoadCharacters()
    {
        Canvas[] tempPlayerUI = Hud.GetComponentsInChildren<Canvas>();    
        //P1
        Character temp = Instantiate(Character1, transform);
        temp.Initialise(Character.EType.ePlayer, tempPlayerUI[1]);
        mCharacters.Add(temp);
        //P2
        temp = Instantiate(Character2, transform);
        temp.Initialise(Character.EType.ePlayer, tempPlayerUI[2]);
        mCharacters.Add(temp);
        //P3
        temp = Instantiate(Character3, transform);
        temp.Initialise(Character.EType.ePlayer, tempPlayerUI[3]);
        mCharacters.Add(temp);
        //P4
        temp = Instantiate(Character4, transform);
        temp.Initialise(Character.EType.ePlayer, tempPlayerUI[4]);
        mCharacters.Add(temp);
        //Enemy
        temp = Instantiate(Enemy, transform);
        temp.CharacterType = Character.EType.eEnemy;
        mCharacters.Add(temp);
    }

    public void LoadItems()
    {
        //Key
        Item temp = Instantiate(Key, transform);
        temp.ItemType = Item.EItemType.eKey;
        Renderer tempRender = temp.GetComponentInChildren<Renderer>();
        tempRender.enabled = false;
        mItems.Add(temp);
        //Heart
        for (int i = 0; i < 3; i++)
        {
            temp = Instantiate(Heart, transform);
            temp.ItemType = Item.EItemType.eHeart;
            tempRender = temp.GetComponentInChildren<Renderer>();
            tempRender.enabled = false;
            mItems.Add(temp);
        }        
        //Chest
        temp = Instantiate(Chest, transform);
        temp.ItemType = Item.EItemType.eChest;
        Renderer[] tmpRn = temp.GetComponentsInChildren<Renderer>();
        foreach (Renderer rn in tmpRn)
        {
            rn.enabled = false;
        }
        mItems.Add(temp);        
        for (int i = 0; i < 20; i++)
        {
            //Jewel
            temp = Instantiate(Jewel, transform);
            temp.ItemType = Item.EItemType.eJewel;
            tempRender = temp.GetComponentInChildren<Renderer>();
            tempRender.enabled = false;
            mItems.Add(temp);            
        }        
    }
    public void Generate()
    {
        mMap.GenerateWorld();
        foreach (Item item in mItems)
        {
            EnvironmentTile temp = mMap.FindAvailableTile();           
            item.transform.position = temp.Position;
            item.CurrentPosition = temp;
            Renderer[] tmpRn = item.GetComponentsInChildren<Renderer>();
            foreach (Renderer rn in tmpRn)
            {
                rn.enabled = true;
            }
            if (item.name.Contains("Key"))
            {
                item.transform.position = mMap.Middle.Position;
                item.CurrentPosition = mMap.Middle;
            }
        }        
    }

    public void Exit()
    {
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }
}
