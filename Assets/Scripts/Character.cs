﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    public List<EnvironmentTile> mRoute { get; set; }
    public enum EType { ePlayer, eEnemy};
    [SerializeField] private float SingleNodeMoveTime = 0.5f;
    public EnvironmentTile CurrentPosition { get; set; }
    public EnvironmentTile PreviousPosition { get; set; }
    public EType CharacterType { get; set; }
    public Canvas UI { get; set; }
    public bool isAlive { get; set; } = true;
    public bool hasTheKey { get; private set; } = false;
    public bool canRevive { get; private set; } = false;

    private IEnumerator DoMove(Vector3 position, Vector3 destination)
    {
        // Move between the two specified positions over the specified amount of time
        if (position != destination)
        {
            transform.rotation = Quaternion.LookRotation(destination - position, Vector3.up);

            Vector3 p = transform.position;
            float t = 0.0f;

            while (t < SingleNodeMoveTime)
            {
                t += Time.deltaTime;
                p = Vector3.Lerp(position, destination, t / SingleNodeMoveTime);
                transform.position = p;
                yield return null;
            }
        }           
    }

    private IEnumerator DoGoTo(List<EnvironmentTile> route)
    {
        // Move through each tile in the given route
        if (route != null)
        {
            Vector3 position = CurrentPosition.Position;
            for (int count = 0; count < route.Count; ++count)
            {
                Vector3 next = route[count].Position;
                yield return DoMove(position, next);
                PreviousPosition = CurrentPosition;
                CurrentPosition = route[count];
                position = next;
            }
        }
    }

    public void GoTo(List<EnvironmentTile> route)
    {
        // Clear all coroutines before starting the new route so 
        // that clicks can interupt any current route animation
        StopAllCoroutines();
        StartCoroutine(DoGoTo(route));
    }

    public bool CheckCollisions(Character other)
    {
        if(CurrentPosition != null)
        {          
            if (CurrentPosition == other.CurrentPosition && CharacterType != other.CharacterType && CharacterType == EType.ePlayer)
            {
                isAlive = false;
                Image[] tempImages = UI.GetComponentsInChildren<Image>();
                foreach (Image image in tempImages)
                {
                    if (image.name.Contains("Death"))
                    {
                        image.enabled = true;
                        break;
                    }
                }
                return true;
            }
            if (CurrentPosition == other.CurrentPosition && CharacterType == other.CharacterType)
            {
                if (!isAlive && other.canRevive)
                {
                    isAlive = true;
                    other.ReviveCompanion();
                    Image[] tempImages = UI.GetComponentsInChildren<Image>();
                    foreach (Image image in tempImages)
                    {
                        if (image.name.Contains("Death"))
                        {
                            image.enabled = false;
                            break;
                        }
                    }
                }
                else if (!other.isAlive && canRevive)
                {
                    other.isAlive = true;
                    ReviveCompanion();
                    Image[] tempImages = other.UI.GetComponentsInChildren<Image>();
                    foreach (Image image in tempImages)
                    {
                        if (image.name.Contains("Death"))
                        {
                            image.enabled = false;
                            break;
                        }
                    }
                }                
                return true;
            }
        }
        return false;
    }

    public bool CheckCollisions(Item other)
    {
        if (CurrentPosition != null)
        {
            Renderer tempRender = other.GetComponentInChildren<Renderer>();
            if (CurrentPosition == other.CurrentPosition && CharacterType == EType.ePlayer && tempRender.enabled)
            {
                if (other.ItemType == Item.EItemType.eKey)
                {
                    hasTheKey = true;
                    Image[] tempImages = UI.GetComponentsInChildren<Image>();
                    foreach (Image image in tempImages)
                    {
                        if (image.name.Contains("Key"))
                        {
                            image.enabled = true;
                            break;
                        }
                    }
                }                  
                if (other.ItemType == Item.EItemType.eHeart)
                {
                    Image[] tempImages = UI.GetComponentsInChildren<Image>();
                    foreach (Image image in tempImages)
                    {
                        if (image.name.Contains("Heart") && !image.enabled)
                        {
                            image.enabled = true;
                            canRevive = true;
                            break;
                        }
                    }
                }               
                if(other.ItemType != Item.EItemType.eChest)
                {
                    tempRender.enabled = false;
                }
                return true;
            }
        }
        return false;
    }
    public void Initialise(Character.EType argType, Canvas argCanvas)
    {
        CharacterType = argType;
        UI = argCanvas;
        ResetValues();
    }
    public void ReviveCompanion()
    {
        Image[] tempImages = UI.GetComponentsInChildren<Image>();
        foreach (Image image in tempImages)
        {
            if (image.name.Contains("Heart") && image.enabled)
            {
                image.enabled = false;
                canRevive = false;
                break;
            }
        }
        foreach (Image image in tempImages)
        {
            if (image.name.Contains("Heart") && image.enabled)
            {
                canRevive = true;
                break;
            }
        }
    }
    public void ResetValues()
    {
        isAlive = true;
        canRevive = false;
        hasTheKey = false;
        Image[] tempImages = UI.GetComponentsInChildren<Image>();
        foreach (Image image in tempImages)
        {
            if (image.name.Contains("Key") || image.name.Contains("Heart") || image.name.Contains("Death"))
            {
                image.enabled = false;
            }
        }
    }
}
